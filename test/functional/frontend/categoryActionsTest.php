<?php

include(dirname(__FILE__) . '/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
get('/category/index')->

with('request')->begin()->
isParameter('module', 'category')->
isParameter('action', 'index')->
end()->

with('response')->begin()->
isStatusCode(200)->
checkElement('body', '!/This is a temporary page/')->
end();

$browser->info('1 - The homepage')->
get('/')->
info(sprintf('  1.2 - Only %s jobs are listed for a category', $max))->
with('response')->
checkElement('.category_programming tr', $max);

$browser->info('1 - The homepage')->
get('/')->
info('  1.3 - A category has a link to the category page only if too many jobs')->
with('response')->begin()->
checkElement('.category_design .more_jobs', false)->
checkElement('.category_programming .more_jobs')->
end();

$q = Doctrine_Query::create()
    ->select('j.*')
    ->from('JobeetJob j')
    ->leftJoin('j.JobeetCategory c')
    ->where('c.slug = ?', 'programming')
    ->andWhere('j.expires_at > ?', date('Y-m-d', time()))
    ->orderBy('j.created_at DESC');

$job = $q->fetchOne();

$browser->info('1 - The homepage')->
get('/')->
info('  1.4 - Jobs are sorted by date')->
with('response')->begin()->
checkElement(sprintf('.category_programming tr:first a[href*="/%d/"]', $job->getId()))->
end();

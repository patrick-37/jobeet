<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php include_slot('title', 'Jobeet - Your best job board') ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>
</head>
<body>
<nav class="navbar is-dark" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="<?php echo url_for('homepage') ?>">
            <h1>Jobeet</h1>
        </a>

        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
            <!-- <a class="navbar-item">
                Home
            </a>

            <a class="navbar-item">
                Documentation
            </a> -->
            </div>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="buttons">
                    <a href="<?php echo url_for('job_new') ?>" class="button is-primary">
                        <strong>Post a Job</strong>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
<section class="section">
    <div class="container content">
        <div class="search">
            <h2>Ask for a job</h2>
            <form action="" method="get">
                <div class="block">
                <input type="text" name="keywords"
                       id="search_keywords" class="input"/>
                    <div class="help">
                        Enter some keywords (city, country, position, ...)
                    </div>
                </div>
                <div class="block">
                    <input type="submit" class="button is-dark" value="search" />
                </div>
            </form>
        </div>
        <div id="content" class="block mt-5">
        <?php if ($sf_user->hasFlash('notice')): ?>
            <div class="flash_notice">
                <?php echo $sf_user->getFlash('notice') ?>
            </div>
        <?php endif ?>

        <?php if ($sf_user->hasFlash('error')): ?>
            <div class="flash_error">
                <?php echo $sf_user->getFlash('error') ?>
            </div>
        <?php endif ?>

        <div class="content block">
            <?php echo $sf_content ?>
        </div>
    </div>
</section>
<footer class="container">
    <div class="content">
        <div class="is-flex">
            <a href="#" class="p-2">About Jobeet</a>
            <a href="#" class="p-2"> Full feed</a>
            <a href="#" class="p-2">Jobeet API</a>
            <a href="#" class="p-2">Affiliates</a>
        </div>
    </div>
</footer>
</body>
</html>